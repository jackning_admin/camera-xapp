package com.example.cameraxapp.app

import android.app.Application

/**
 * @author:njb
 * @date: 2023/10/15 14:22
 * @desc:描述
 **/
class CameraApp :Application(){

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object{
        lateinit var instance: CameraApp
    }
}