package com.example.cameraxapp

import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_photo.*

class PhotoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)
        initView()
    }

    private fun initView() {
        openAlbum.setOnClickListener {
            photoLaunch.launch("image/*")
        }
    }

    private val photoLaunch =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            Glide.with(this@PhotoActivity).load(uri.toString()).into(iv_photo)
        }
}